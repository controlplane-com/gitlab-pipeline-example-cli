# Control Plane - GitLab Pipeline Example Using the CLI

This example demonstrates building and deploying an app to Control Plane using the CLI as part of a CI/CD pipeline.

The example is a Node.js app that displays the environment variables and start-up arguments.

This example is provided as a starting point and your own unique delivery and/or deployment requirements will dictate the steps needed in your situation.

## Control Plane Authentication Set Up 

The Control Plane CLI requires a `Service Account` with the proper permissions to perform actions against the Control Plane API. 

1. Follow the Control Plane documentation to create a Service Account and create a key. Take a note of the key. It will be used in the next section.
2. Add the Service Account to the `superusers` group. Once the pipeline executes as expected, a policy can be created with a limited set of permissions and the Service Account can be removed from the `superusers` group.


## Example Set Up

When triggered, the pipeline will execute the stages defined in the file located at `.gitlab-ci.yml`. The example will containerize and push the application to the org's private image repository and create/update a GVC and workload hosted at Control Plane. Any committed modifications to the repository will trigger the pipeline.

**Perform the following steps to set up the example:**

1. Fork the example into your own workspace.

2. The following variables are required and must be added as GitLab repository variables.
   
Browse to the variables page by:
- Clicking `Settings` (left menu bar), then click `CI/CD`.
- Click the `Expand` button within the `Variables` section.

Add the following variables:
- `CPLN_ORG`: Control Plane org.
- `CPLN_GVC_NAME`: The name of the GVC.
- `CPLN_WORKLOAD_NAME`: The name of the workload.
- `CPLN_TOKEN` Service Account Key from the previous section (masked).
- `CPLN_IMAGE_NAME`: The name of the image that will be deployed. The pipeline will append the short SHA of the commit as the tag when pushing the image to the org's private image repository.

3. Review, update, and commit the `.gitlab-ci.yml` file
- If needed, update the pipeline steps to be triggered on specific branch actions (pushes, pull requests, etc.) and environments. The `deploy` stage is triggered manually.
- The `sed` command is used to substitute the `ORG_NAME`, `GVC_NAME`, `WORKLOAD_NAME` and `IMAGE_NAME_TAG` tokens inside the YAML files in the `/cpln` directory on lines 25-28.

4. The Control Plane YAML files are located in the `/cpln` directory. No changes are required to execute the example.
- The `cpln-gvc.yaml` file defines the GVC to be created/updated.
- The `cpln-workload.yaml` file defines the workload to be created/updated. 

5. Run the pipeline by:
- Click `CI/CD` in the left menu, then click `Pipelines`.
- Click `Run pipeline` in the upper right corner, then click the `Run pipeline` button that is in the center of the page.


## Running the App

After the pipeline has successfully deployed the application, it can be tested by following these steps:

1. Browse to the Control Plane Console.
2. Select the GVC that was set in the `CPLN_GVC_NAME` variable.
3. Select the workload that was set in the `CPLN_WORKLOAD_NAME` variable.
4. Click the `Open` button. The app will open in a new tab. The container's environment variables and start up arguments will be displayed.



## Notes

- The `cpln apply` command creates and updates the resources defined within the YAML files. If the name of a resource is changed, `cpln apply` will create a new resource. Any orphaned resources will need to be manually deleted.

- The Control Plane CLI commands will use the `CPLN_ORG` and `CPLN_TOKEN` environment variables when needed. There is no need to add the --org or --token flags.

- The GVC definition must exists in its own YAML file. The `cpln apply` command executing the file that contains the GVC definition must be executed before any child definition YAML files (workloads, identities, etc.) are executed.

## Helper Links

GitLab

- <a href="https://docs.gitlab.com/ee/ci/" target="_blank">GitLab CI/CD Docs</a>

